/****************************************************************************
 **
 ** This file is part of the yFiles extension package GraphML-3.2-yFiles-2.7.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Redistribution of this file or of an unauthorized byte-code version
 ** of this file is strictly forbidden.
 **
 ** Copyright (c) 2000-2009 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/

package demo.yext.graphml;

import y.view.Arrow;
import yext.graphml.compat.GraphMLLayoutGraphIOHandler;
import yext.graphml.graph2D.GraphMLIOHandler;
import org.graphdrawing.graphml.GraphMLConstants;

/**
 * Simple demo that shows how to read and write basic layout information in .NET GraphML dialect.
 */
public class DotnetCompatDemo extends GraphMLDemo {


  public DotnetCompatDemo() {
    view.getGraph2D().getDefaultEdgeRealizer().setTargetArrow(Arrow.STANDARD);
  }

  protected GraphMLIOHandler createGraphMLIOHandler() {
    GraphMLLayoutGraphIOHandler graphMLLayoutGraphIOHandler = new GraphMLLayoutGraphIOHandler();
    graphMLLayoutGraphIOHandler.setIgnoreGraphicsEnabled(true);
    graphMLLayoutGraphIOHandler.addNamespace(GraphMLConstants.YWORKS_EXT_NS_URI, "y");
    return graphMLLayoutGraphIOHandler;
  }

  /**
   * Launches this demo.
   */
  public static void main(String[] args) {
    initLnF();
    final DotnetCompatDemo demo = new DotnetCompatDemo();
    demo.start();
  }
}
