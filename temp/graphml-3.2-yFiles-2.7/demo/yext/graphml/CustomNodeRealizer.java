/****************************************************************************
 **
 ** This file is part of the yFiles extension package GraphML-3.2-yFiles-2.7.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Redistribution of this file or of an unauthorized byte-code version
 ** of this file is strictly forbidden.
 **
 ** Copyright (c) 2000-2009 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/

package demo.yext.graphml;

import y.view.NodeRealizer;
import y.view.ShapeNodeRealizer;

import java.awt.Graphics2D;
import java.awt.Color;

/**
 * A simple customization of {@link y.view.ShapeNodeRealizer} that holds additional
 * fields.
 * GraphML serialization of this realizer and its additional fields is handled by
 * {@link demo.yext.graphml.CustomNodeRealizerSerializer}.
 */
public class CustomNodeRealizer extends ShapeNodeRealizer
{
  // Custom value
  public int customValue;
  // Custom attribute
  public String customAttribute;

  /** Creates a new instance of CustomNodeRealizer. */
  public CustomNodeRealizer() {
    setSize(60,40);
    customAttribute = "v1.0";
    customValue = 333;
  }

  /** Creates a new instance of CustomNodeRealizer. */
  public CustomNodeRealizer(NodeRealizer nr)
  {
    super(nr);
    // If the given node realizer is of this type, then apply copy semantics. 
    if (nr instanceof CustomNodeRealizer)
    {
      CustomNodeRealizer fnr = (CustomNodeRealizer)nr;
      // Copy the values of custom attributes. 
      customValue = fnr.customValue;
      customAttribute = fnr.customAttribute;
    }
  }

  public NodeRealizer createCopy(NodeRealizer nr)
  {
    return new CustomNodeRealizer(nr);
  }

  public void paintText(Graphics2D gfx) {
    super.paintText(gfx);
    gfx.setColor(Color.blue);
    gfx.drawString("value: " + customValue, (float)getX()+ 4, (float)getY()+12);
    gfx.drawString("attr:  " + customAttribute, (float)getX()+ 4, (float)(getY()+ getHeight()-2));
  }

}
