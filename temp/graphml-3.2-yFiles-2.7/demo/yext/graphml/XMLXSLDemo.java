/****************************************************************************
 **
 ** This file is part of the yFiles extension package GraphML-3.2-yFiles-2.7.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Redistribution of this file or of an unauthorized byte-code version
 ** of this file is strictly forbidden.
 **
 ** Copyright (c) 2000-2009 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/

package demo.yext.graphml;


import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;

import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.xml.transform.stream.StreamSource;

import y.util.D;
import y.view.Graph2D;
import yext.graphml.graph2D.GraphMLIOHandler;
import yext.graphml.graph2D.XMLXSLIOHandler;


/**
 This demo shows how XML files can imported as
 GraphML by the means of an XSLT stylesheet. 
 Sample stylesheets for the following XML data are provided:
 <ul>
 	<li><a href="resources/xsl/ant2graphml.xsl">Ant build scripts</a></li>
 	<li><a href="resources/xsl/owl2graphml.xsl">OWL web ontology data</a></li>
 	<li><a href="resources/xsl/xmltree2graphml.xsl">the XML tree structure</a></li>
 </ul>
 */
public class XMLXSLDemo extends GraphMLDemo
{
  
  /** 
   * Creates a new instance of XMLXSLDemo 
   */
  public XMLXSLDemo()
  {
  }

  protected JMenu createSampleGraphMenu() {
    JMenu menu = new JMenu("XML/XSL Samples");
    String[][] resources = {
            {"resources/xml/ant-build.xml",
             "resources/xsl/ant2graphml.xsl"},
            {"resources/xml/food.owl",
             "resources/xsl/owl2graphml.xsl"},
            {"resources/xml/food.owl",
             "resources/xsl/xmltree2graphml.xsl"},
    };
    int resolvedResourceCount = 0;
    for (int i = 0; i < resources.length; ++i) {
      final URL xml = getClass().getResource(resources[i][0]);
      final URL xsl = getClass().getResource(resources[i][1]);
      if (xml != null && xsl != null) {
        ++resolvedResourceCount;
        menu.add(new LoadXMLPlusXSLAction(xml, xsl));
      } else {
        if (xml == null) {
          System.err.println("Could not resolve sample resource " + resources[i][0]);
        }
        if (xsl == null) {
          System.err.println("Could not resolve sample resource " + resources[i][1]);
        }
      }
    }
    if (resolvedResourceCount > 0) {
      return menu;
    } else {
      return null;
    }
  }
  
  /**
   * Action that loads a graph from a specific file in GraphML format.
   */
  class LoadXMLPlusXSLAction extends AbstractAction
  {
    URL xmlResource, xslResource;
    LoadXMLPlusXSLAction(URL xmlResource, URL xslResource)
    {
      putValue(AbstractAction.NAME,  
          "XML:" + urlToName(xmlResource) + "  XSL:" + urlToName(xslResource));
      this.xmlResource = xmlResource;
      this.xslResource = xslResource;
    }
  
    String urlToName(URL url)
    {
      String file = url.getFile();
      return file.substring(file.lastIndexOf('/')+1);
    }
      
    public void actionPerformed(ActionEvent ev)
    {
      try
      {
        Graph2D graph = view.getGraph2D();
        graph.clear();
        GraphMLIOHandler graphml = new GraphMLIOHandler();
        XMLXSLIOHandler ioh = new XMLXSLIOHandler(graphml);
        
        ioh.setXSLSource(new StreamSource(xslResource.openStream()));
        ioh.read(graph, xmlResource);
        view.fitContent();
        view.updateView();
      }
      catch (IOException ioe)
      {
        D.show(ioe);
      }
    }
  }
  
  /**
   * Launches this demo.
   */
  public static void main(String[] args)
  {
    initLnF();
    final XMLXSLDemo demo = new XMLXSLDemo();
    demo.start();
  }
}
