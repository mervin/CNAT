/****************************************************************************
 **
 ** This file is part of the yFiles extension package GraphML-3.2-yFiles-2.7.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Redistribution of this file or of an unauthorized byte-code version
 ** of this file is strictly forbidden.
 **
 ** Copyright (c) 2000-2009 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/

package demo.yext.graphml;

import org.graphdrawing.graphml.reader.dom.DOMGraphMLParseContext;
import org.graphdrawing.graphml.reader.dom.DOMInputHandler;
import org.graphdrawing.graphml.reader.dom.Precedence;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import y.util.D;
import yext.graphml.graph2D.GraphMLIOHandler;

import javax.swing.JMenu;

/**
 * A simple customization of {@link GraphMLDemo} that uses objects of type 
 * {@link CustomNodeRealizer} as the graph's default node realizer. 
 * To enable encoding and parsing of this node realizer type a specific serializer 
 * implementation is registered with GraphMLIOHandler. 
 */
public class CustomNodeRealizerSerializerDemo extends GraphMLDemo {
  /**
   * Creates a new instance of CustomNodeRealizerSerializerDemo.
   */
  public CustomNodeRealizerSerializerDemo() {
    // Use another default node realizer. 
    view.getGraph2D().setDefaultNodeRealizer(new CustomNodeRealizer());
  }

  protected JMenu createSampleGraphMenu() {
    return createSampleGraphMenu(new String[]{"resources/custom/custom-noderealizer-serializer.graphml"});
  }

  protected GraphMLIOHandler createGraphMLIOHandler() {
    GraphMLIOHandler ioHandler = new GraphMLIOHandler();
    // Register the node realizer's specific serializer that knows how to encode 
    // valid XML markup and also how to parse the encoded data.
    ioHandler.getRealizerSerializerManager().addNodeRealizerSerializer(new CustomNodeRealizerSerializer());
    return ioHandler;
  }

  /**
   * Launches this demo. 
   */
  public static void main(String[] args) {
    initLnF();
    final CustomNodeRealizerSerializerDemo demo = new CustomNodeRealizerSerializerDemo();
    demo.start();
  }
}
