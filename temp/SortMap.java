package util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * 
 *   SortMap.java
 * 将Map<Number, Number>,按照key 或者value值进行排序   
 *  @author Mervin.Wong  DateTime 2013-6-9 上午9:36:59    
 *  @version 0.4.0
 */
public class SortMap {
	/*
	 * 需要排序的map
	 */
	private Map<Number, Number> map = null;
	public SortMap(){
		
	}
	public SortMap(Map<Number, Number> map){
		this.map = map;
	}
	/**
	 * 进行排序
	 *  
	 *  @param key true: 对key; flase:对value值排序
	 *  @param des true：降序；false:升序
	 */
	public PairList<Number, Number> sort(){
		return this.sort(this.map);
	}	
	/**
	 * 进行排序
	 *  
	 *  @param key true: 对key; flase:对value值排序
	 *  @param des true：降序；false:升序
	 */
	public PairList<Number, Number> sort(final boolean key, final boolean des){
		return this.sort(this.map, key, des);
	}
	/**
	 * 进行排序
	 *  
	 *  @param key true: 对key; flase:对value值排序
	 *  @param des true：降序；false:升序
	 */
	public PairList<Number, Number> sort(Map<Number, Number> map){
		PairList<Number, Number> pl = new PairList<Number, Number>();
		Set<Map.Entry<Number, Number>> sortedSet = this.set(true, true);
		sortedSet.addAll(map.entrySet());
		for (Iterator<Entry<Number, Number>> iterator = sortedSet.iterator(); iterator.hasNext();) {
			Entry<Number, Number> entry = (Entry<Number, Number>) iterator.next();
			pl.add(entry.getKey(), entry.getValue());
		}
		return pl;
	}	
	/**
	 * 进行排序
	 *  
	 *  @param key true: 对key; flase:对value值排序
	 *  @param des true：降序；false:升序
	 */
	public PairList<Number, Number> sort(Map<Number, Number> map, final boolean key, final boolean des){
		PairList<Number, Number> pl = new PairList<Number, Number>();
		Set<Map.Entry<Number, Number>> sortedSet = this.set(key, des);
		sortedSet.addAll(map.entrySet());
		for (Iterator<Entry<Number, Number>> iterator = sortedSet.iterator(); iterator.hasNext();) {
			Entry<Number, Number> entry = (Entry<Number, Number>) iterator.next();
			pl.add(entry.getKey(), entry.getValue());
		}
		return pl;
	}
	
	/*
	 * 设置按key或者value排序：升序或者降序
	 */
	private SortedSet<Map.Entry<Number, Number>> set(final boolean key, final boolean asc){
		SortedSet<Map.Entry<Number, Number>> sortedSet = new TreeSet<Map.Entry<Number, Number>>(
	            new Comparator<Map.Entry<Number, Number>>() {
	                @Override
	                public int compare(Map.Entry<Number, Number> e1, Map.Entry<Number, Number> e2) {
	                	
	                	if(key){
	                		//对key排序
	                		long k1 = e1.getKey().longValue();
	                		long k2 = e2.getKey().longValue();
	                		if(asc){
	                			//升序
	                			if(k1 > k2){
	                				return 1;
	                			}else{
	                				return -1;
	                			}
	                		}else{
	                			//降序
	                			if(k1 < k2){
	                				return 1;
	                			}else{
	                				return -1;
	                			}	                			
	                		}
	                	}else{
	                		//对value排序
	                		long k1 = e1.getValue().longValue();
	                		long k2 = e2.getValue().longValue();
	                		if(asc){
	                			//升序
	                			if(k1 > k2){
	                				return 1;
	                			}else{
	                				return -1;
	                			}
	                		}else{
	                			//降序
	                			if(k1 < k2){
	                				return 1;
	                			}else{
	                				return -1;
	                			}	                			
	                		}	                		
	                	}
	                }
	            });
		return sortedSet;
	}
	/*
	 *  
	 *  @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
